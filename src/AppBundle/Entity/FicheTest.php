<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FicheTest
 *
 * @ORM\Table(name="fiche_test")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FicheTestRepository")
 */
class FicheTest
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, unique=true)
     */
    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity="TicketMantis", inversedBy="fiches")
     * @ORM\JoinColumn(name="ticket_mantis_id", referencedColumnName="id")
     */
    private $ticketMantis;


    /**
     * @ORM\ManyToOne(targetEntity="Module", inversedBy="fiches")
     * @ORM\JoinColumn(name="module_id", referencedColumnName="id")
     */
    private $module;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return FicheTest
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set ticketMantis
     *
     * @param \AppBundle\Entity\TicketMantis $ticketMantis
     *
     * @return FicheTest
     */
    public function setTicketMantis(\AppBundle\Entity\TicketMantis $ticketMantis = null)
    {
        $this->ticketMantis = $ticketMantis;

        return $this;
    }

    /**
     * Get ticketMantis
     *
     * @return \AppBundle\Entity\TicketMantis
     */
    public function getTicketMantis()
    {
        return $this->ticketMantis;
    }


    /**
     * Set module
     *
     * @param \AppBundle\Entity\Module $module
     *
     * @return FicheTest
     */
    public function setModule(\AppBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return \AppBundle\Entity\Module
     */
    public function getModule()
    {
        return $this->module;
    }
}
