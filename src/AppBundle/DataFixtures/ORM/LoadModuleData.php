<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\FicheTest;
use AppBundle\Entity\Module;
use AppBundle\Entity\TicketMantis;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 19/07/2016
 * Time: 00:00
 */
class LoadModuleData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $module = new Module();
        $module->setNom('Test Module');

        $manager->persist($module);
        $manager->flush();

        $this->addReference('first_module', $module);

        /* ERROR
        [SymfonyComponentDebugExceptionClassNotFoundException]
         Attempted to load class "ArrayCollection" from namespace "AppBundleEntity".
         Did you forget a "use" statement for "DoctrineCommonCollectionsArrayCollection"? */

    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
/*    public function getOrder()
    {
        // TODO: Implement getOrder() method.
         return 1;

    }*/
}
