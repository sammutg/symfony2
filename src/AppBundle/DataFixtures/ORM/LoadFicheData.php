<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\FicheTest;
use AppBundle\Entity\Module;
use AppBundle\Entity\TicketMantis;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 19/07/2016
 * Time: 00:00
 */
class LoadFicheData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $fiche = new FicheTest();
        $fiche->setNom('Ma première fiche');


        $fiche->setModule($this->getReference(
            'first_module'
        ));
        $fiche->setTicketMantis($this->getReference(
            'first_ticket'
        ));


        $manager->persist($fiche);
        $manager->flush();
    }

    public function getOrder() {
        return 3;
    }

}
