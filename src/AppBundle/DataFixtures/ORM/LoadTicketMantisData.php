<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\FicheTest;
use AppBundle\Entity\Module;
use AppBundle\Entity\TicketMantis;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 19/07/2016
 * Time: 00:00
 */
class LoadTicketMantisData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $ticket = new TicketMantis();
        $ticket->setDescription('Mon premier ticket');
        $ticket->setNumero('1200');

        $manager->persist($ticket);
        $manager->flush();

        $this->addReference('first_ticket', $ticket);

    }

    public function getOrder() {
        return 2;
    }

}
