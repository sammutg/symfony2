<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\TicketMantis;
use AppBundle\Form\TicketMantisType;

/**
 * TicketMantis controller.
 *
 * @Route("/ticketmantis")
 */
class TicketMantisController extends Controller
{
    /**
     * Lists all TicketMantis entities.
     *
     * @Route("/", name="ticketmantis_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $ticketMantis = $em->getRepository('AppBundle:TicketMantis')->findAll();

        return $this->render('ticketmantis/index.html.twig', array(
            'ticketMantis' => $ticketMantis,
        ));
    }

    /**
     * Creates a new TicketMantis entity.
     *
     * @Route("/new", name="ticketmantis_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $ticketManti = new TicketMantis();
        $form = $this->createForm('AppBundle\Form\TicketMantisType', $ticketManti);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ticketManti);
            $em->flush();

            return $this->redirectToRoute('ticketmantis_show', array('id' => $ticketManti->getId()));
        }

        return $this->render('ticketmantis/new.html.twig', array(
            'ticketManti' => $ticketManti,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TicketMantis entity.
     *
     * @Route("/{id}", name="ticketmantis_show")
     * @Method("GET")
     */
    public function showAction(TicketMantis $ticketManti)
    {
        $deleteForm = $this->createDeleteForm($ticketManti);

        return $this->render('ticketmantis/show.html.twig', array(
            'ticketManti' => $ticketManti,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TicketMantis entity.
     *
     * @Route("/{id}/edit", name="ticketmantis_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TicketMantis $ticketManti)
    {
        $deleteForm = $this->createDeleteForm($ticketManti);
        $editForm = $this->createForm('AppBundle\Form\TicketMantisType', $ticketManti);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ticketManti);
            $em->flush();

            return $this->redirectToRoute('ticketmantis_edit', array('id' => $ticketManti->getId()));
        }

        return $this->render('ticketmantis/edit.html.twig', array(
            'ticketManti' => $ticketManti,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a TicketMantis entity.
     *
     * @Route("/{id}", name="ticketmantis_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TicketMantis $ticketManti)
    {
        $form = $this->createDeleteForm($ticketManti);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($ticketManti);
            $em->flush();
        }

        return $this->redirectToRoute('ticketmantis_index');
    }

    /**
     * Creates a form to delete a TicketMantis entity.
     *
     * @param TicketMantis $ticketManti The TicketMantis entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TicketMantis $ticketManti)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ticketmantis_delete', array('id' => $ticketManti->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
