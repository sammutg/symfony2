<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\FicheTest;
use AppBundle\Form\FicheTestType;

/**
 * FicheTest controller.
 *
 * @Route("/fichetest")
 */
class FicheTestController extends Controller
{
    /**
     * Lists all FicheTest entities.
     *
     * @Route("/", name="fichetest_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $ficheTests = $em->getRepository('AppBundle:FicheTest')->findAll();

        return $this->render('fichetest/index.html.twig', array(
            'ficheTests' => $ficheTests,
        ));
    }

    /**
     * Creates a new FicheTest entity.
     *
     * @Route("/new", name="fichetest_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $ficheTest = new FicheTest();
        $form = $this->createForm('AppBundle\Form\FicheTestType', $ficheTest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ficheTest);
            $em->flush();

            return $this->redirectToRoute('fichetest_show', array('id' => $ficheTest->getId()));
        }

        return $this->render('fichetest/new.html.twig', array(
            'ficheTest' => $ficheTest,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a FicheTest entity.
     *
     * @Route("/{id}", name="fichetest_show")
     * @Method("GET")
     */
    public function showAction(FicheTest $ficheTest)
    {
        $deleteForm = $this->createDeleteForm($ficheTest);

        return $this->render('fichetest/show.html.twig', array(
            'ficheTest' => $ficheTest,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing FicheTest entity.
     *
     * @Route("/{id}/edit", name="fichetest_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, FicheTest $ficheTest)
    {
        $deleteForm = $this->createDeleteForm($ficheTest);
        $editForm = $this->createForm('AppBundle\Form\FicheTestType', $ficheTest);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ficheTest);
            $em->flush();

            return $this->redirectToRoute('fichetest_edit', array('id' => $ficheTest->getId()));
        }

        return $this->render('fichetest/edit.html.twig', array(
            'ficheTest' => $ficheTest,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a FicheTest entity.
     *
     * @Route("/{id}", name="fichetest_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, FicheTest $ficheTest)
    {
        $form = $this->createDeleteForm($ficheTest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($ficheTest);
            $em->flush();
        }

        return $this->redirectToRoute('fichetest_index');
    }

    /**
     * Creates a form to delete a FicheTest entity.
     *
     * @param FicheTest $ficheTest The FicheTest entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(FicheTest $ficheTest)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('fichetest_delete', array('id' => $ficheTest->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
