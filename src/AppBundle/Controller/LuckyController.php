<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

// --> add this new use statement
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Description of LuckyController
 *
 * @author myje
 */
class LuckyController extends Controller {

    /**
     * @Route("/lucky/number")
     */
    public function numberAction()
    {
        $number = rand(0, 100);
//        dump($number);

        return $this->render('number.html.twig', array('number' => $number ));
    
    }
}